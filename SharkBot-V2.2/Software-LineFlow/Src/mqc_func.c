#include "mqc_func.h"
#include "led.h"
#include "main.h"
#include "cmsis_os.h"
#include "math.h"
#include <stdlib.h>

/*
File:           mqc_func.c
Funtion:        User funtion 
Decription:     This funtion will be control 7SEG LED 
Author:         MQ-Crexcel
Date modefine:  30/03/21
*/


#define IN_LINE 0
#define OUT_LINE 1
#define STOP_LINE 2

extern uint8_t mqc_Butt_Mode;
extern uint8_t mqc_Butt_Select;
extern uint8_t mqc_Cnt_Mode;
extern volatile uint16_t mqc_Analog_Val[10];
uint16_t mqc_Ana_Val_Lo[10];
uint16_t mqc_Ana_Val_Hi[10];
uint16_t mqc_Ana_Val_Mid[10];
uint8_t mqc_Lock_Mode = 0;
uint8_t mqc_Lock_Lo = 0;
uint8_t mqc_Lock_Hi = 0;
uint8_t mqc_Lock_Show = 0;
float mqc_Err_Lelf = 0;
float mqc_Err_Right = 0;
int mqc_Speed_Lelf = 0;
int mqc_Speed_Right = 0;
char mqc_show = ' ';
int mqc_Error, mqc_Last_Error;
int mqc_Real_Cal = 0;
int mqc_Line_Status= 0;

/*Control Motor A 
  dir = 1 run forward, dir = 0 run revert
  pwm min = 0, pwm max = 500
*/
void mqc_Set_MotorA(int dir, int pwm)
{
	if(pwm < 0)
	{
		pwm = 0;
	}	
	
	if(pwm > 500)
	{
		pwm = 500;
	}		
	
	else
	{
	//Do nothing
	}
	
	if(1 == dir)
	{
		TIM2 ->CCR1 = pwm;
		TIM2 ->CCR2 = 0;	
	}
	if(0 == dir)
	{
		TIM2 ->CCR1 = 0;
		TIM2 ->CCR2 = pwm;	
	}
	else
	{
		// Do nothing
	}	
}


/*Control Motor B
  dir = 1 run forward, dir = 0 run revert
  pwm min = 0, pwm max = 500
*/
void mqc_Set_MotorB(int dir, int pwm)
{
	if(pwm < 0)
	{
		pwm = 0;
	}	
	
	if(pwm > 500)
	{
		pwm = 500;
	}		
	
	else
	{
	//Do nothing
	}
	
	if(1 == dir)
	{
		TIM4 ->CCR1 = 0;
		TIM4 ->CCR2 = pwm;	
	}
	if(0 == dir)
	{
		TIM4 ->CCR1 = pwm;
		TIM4 ->CCR2 = 0;	
	}
	else
	{
		// Do nothing
	}	
}

/*Stop Motor A*/
void mqc_Stop_MotorA(void)
{
	TIM2 ->CCR1 = 0;
	TIM2 ->CCR2 = 0;	
}	

/*Stop Motor B*/
void mqc_Stop_MotorB(void)
{
	TIM4 ->CCR1 = 0;
	TIM4 ->CCR2 = 0;		
}

// Please change naming. I will process all button in this funtion.
void mqc_Read_Button(void)
{
	mqc_Butt_Mode = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2);
	mqc_Butt_Select = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_11);	
	if(mqc_Butt_Mode == 0 && mqc_Lock_Mode == 0)
	{
		osDelay(20);
		mqc_Cnt_Mode ++;
		mqc_Lock_Mode = 1;
		if(mqc_Cnt_Mode >5)
		{
			mqc_Cnt_Mode = 0;
		}
	}
	
	if(mqc_Butt_Mode == 1)
	{
		mqc_Lock_Mode = 0;
	}	
}


// Calib sensor 
void mqc_Calib_Line(void)
{
	GPIOA -> BSRR |= GPIO_BSRR_BR12;
	switch (mqc_Cnt_Mode)
	{
		case 1:
		{
			mqc_show = 'L';
			if(mqc_Lock_Lo == 0)
			{
				for(int i=0; i<9; i++)
				{
					mqc_Ana_Val_Lo[i] = mqc_Analog_Val[i] ;
				}
				mqc_Lock_Lo = 1;
			}
			break;
		}
		
		case 2:
		{
			mqc_show = 'h';
			if(mqc_Lock_Hi == 0)
			{
				for(int i=0; i<9; i++)
				{
					mqc_Ana_Val_Hi[i] = mqc_Analog_Val[i] ;
					mqc_Ana_Val_Mid[i] = (mqc_Ana_Val_Hi[i] + mqc_Ana_Val_Lo[i])/2 ;
				}
				mqc_Lock_Hi = 1;
			}
			break;
		}	
	
		case 0:
		{
			mqc_show = ' ';
			break;
		}			

		default:
		{
			mqc_Lock_Lo = 0;
			mqc_Lock_Hi = 0;
			mqc_show = ' ';
		}
	}	
}	

void mqc_Compare_Sensor(void)
{
//	if(mqc_Analog_Val[8] > mqc_Ana_Val_Mid[8])
//	{
//		GPIOA -> BSRR |= GPIO_BSRR_BS8;
//	}
//	else GPIOA -> BSRR |= GPIO_BSRR_BR8;
}


void mqc_Show_Led(void)
{
	Show_7Seg(mqc_show);
	osDelay(5);
  Show_7Seg(' ');
  GPIOA -> BSRR |= GPIO_BSRR_BS12;	
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11,GPIO_PIN_RESET);		 
	
	
  // SS1	
	if(mqc_Analog_Val[8] >= mqc_Ana_Val_Mid[8]){GPIOA -> BSRR |= GPIO_BSRR_BS8;}
	if(mqc_Analog_Val[8] < mqc_Ana_Val_Mid[8]) {GPIOA -> BSRR |= GPIO_BSRR_BR8;}	
	
	// SS2
	if(mqc_Analog_Val[7] >= mqc_Ana_Val_Mid[7]){GPIOA -> BSRR |= GPIO_BSRR_BS9;}
	if(mqc_Analog_Val[7] < mqc_Ana_Val_Mid[7]) {GPIOA -> BSRR |= GPIO_BSRR_BR9;}	
	
	//SS3
	if(mqc_Analog_Val[6] >= mqc_Ana_Val_Mid[6]){GPIOA -> BSRR |= GPIO_BSRR_BS10;}
	if(mqc_Analog_Val[6] < mqc_Ana_Val_Mid[6]) {GPIOA -> BSRR |= GPIO_BSRR_BR10;}	
	
	// SS4
	if(mqc_Analog_Val[4] >= mqc_Ana_Val_Mid[4]){GPIOB -> BSRR |= GPIO_BSRR_BS15;}
	if(mqc_Analog_Val[4] < mqc_Ana_Val_Mid[4]) {GPIOB -> BSRR |= GPIO_BSRR_BR15;}	
	
	// SS5
	if(mqc_Analog_Val[3] >= mqc_Ana_Val_Mid[3]){GPIOB -> BSRR |= GPIO_BSRR_BS14;}
	if(mqc_Analog_Val[3] < mqc_Ana_Val_Mid[3]) {GPIOB -> BSRR |= GPIO_BSRR_BR14;}	
	
	// SS6
	if(mqc_Analog_Val[2] >= mqc_Ana_Val_Mid[2]){GPIOB -> BSRR |= GPIO_BSRR_BS13;}
	if(mqc_Analog_Val[2] < mqc_Ana_Val_Mid[2]) {GPIOB -> BSRR |= GPIO_BSRR_BR13;}		
	
	// SS7
	if(mqc_Analog_Val[1] >= mqc_Ana_Val_Mid[1]){GPIOB -> BSRR |= GPIO_BSRR_BS12;}
	if(mqc_Analog_Val[1] < mqc_Ana_Val_Mid[1]) {GPIOB -> BSRR |= GPIO_BSRR_BR12;}	

	osDelay(5);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11,GPIO_PIN_SET);		 
  GPIOA -> BSRR |= GPIO_BSRR_BR12;
}

void mqc_Calc_Speed(int Kp, int Ki, int Speed)
{
  mqc_Speed_Lelf = Speed + mqc_Err_Lelf * Kp - mqc_Err_Right*Ki ;
  mqc_Speed_Right = Speed + mqc_Err_Right * Kp - mqc_Err_Lelf*Ki;	
	if(mqc_Analog_Val[1] >= mqc_Ana_Val_Mid[1] && mqc_Analog_Val[8] >= mqc_Ana_Val_Mid[8]){mqc_Speed_Lelf = 0; mqc_Speed_Right = 0;}		
	mqc_Set_MotorA(1, mqc_Speed_Right);
	mqc_Set_MotorB(1, mqc_Speed_Lelf);	
}


void mqc_PID(int Kp, int Ki, int Kd, int Speed)
	{
  // SS1	
	if(mqc_Analog_Val[8] >= mqc_Ana_Val_Mid[8])                                             {mqc_Error = 6; mqc_Line_Status = IN_LINE;}	
	// SS1 SS2
	if(mqc_Analog_Val[8] >= mqc_Ana_Val_Mid[8] && mqc_Analog_Val[7] >= mqc_Ana_Val_Mid[7])  {mqc_Error = 5; mqc_Line_Status = IN_LINE;}	
	// SS2
	if(mqc_Analog_Val[7] >= mqc_Ana_Val_Mid[7])                                             {mqc_Error = 4; mqc_Line_Status = IN_LINE;}
	// SS2 SS3
  if(mqc_Analog_Val[7] >= mqc_Ana_Val_Mid[7] && mqc_Analog_Val[6] >= mqc_Ana_Val_Mid[6])  {mqc_Error = 3; mqc_Line_Status = IN_LINE;}		
	//SS3
	if(mqc_Analog_Val[6] >= mqc_Ana_Val_Mid[6])                                             {mqc_Error = 2; mqc_Line_Status = IN_LINE;}
	//SS3 SS4
	if(mqc_Analog_Val[6] >= mqc_Ana_Val_Mid[6] && mqc_Analog_Val[4] >= mqc_Ana_Val_Mid[4])  {mqc_Error = 1; mqc_Line_Status = IN_LINE;}		
	// SS4
	if(mqc_Analog_Val[4] >= mqc_Ana_Val_Mid[4])                                             {mqc_Error = 0; mqc_Line_Status = IN_LINE;}	
	// SS4 SS5 
	if(mqc_Analog_Val[4] >= mqc_Ana_Val_Mid[4] && mqc_Analog_Val[3] >= mqc_Ana_Val_Mid[3])  {mqc_Error = -1;mqc_Line_Status = IN_LINE;}			
	// SS5
	if(mqc_Analog_Val[3] >= mqc_Ana_Val_Mid[3])                                             {mqc_Error = -2;mqc_Line_Status = IN_LINE;}	
	// SS5 SS6
	if(mqc_Analog_Val[3] >= mqc_Ana_Val_Mid[3] && mqc_Analog_Val[2] >= mqc_Ana_Val_Mid[2])  {mqc_Error = -3;mqc_Line_Status = IN_LINE;}				
	// SS6
	if(mqc_Analog_Val[2] >= mqc_Ana_Val_Mid[2])                                             {mqc_Error = -4;mqc_Line_Status = IN_LINE;}
  // SS6 SS7
	if(mqc_Analog_Val[2] >= mqc_Ana_Val_Mid[2] && mqc_Analog_Val[1] >= mqc_Ana_Val_Mid[1])  {mqc_Error = -5;mqc_Line_Status = IN_LINE;}				
	// SS7
	if(mqc_Analog_Val[1] >= mqc_Ana_Val_Mid[1])                                             {mqc_Error = -6;mqc_Line_Status = IN_LINE;}
	 
	if(mqc_Analog_Val[1] >= mqc_Ana_Val_Mid[1] && mqc_Analog_Val[4] >= mqc_Ana_Val_Mid[4] && mqc_Analog_Val[8] >= mqc_Ana_Val_Mid[8])   {mqc_Line_Status = OUT_LINE;}
	
  mqc_Real_Cal = Kp * mqc_Error + Kd * (mqc_Error-mqc_Last_Error);
	mqc_Last_Error = mqc_Error;
	
	if(mqc_Error < 0)
		{
			if(mqc_Line_Status== 1)
				{ 
					/*
					For out of line, the robot need to turn with high rotational acceleration 
					if robot can't go back to line, you can set the Right motor to revert by using 
					mqc_Set_MotorA(0,mqc_Speed_Right) or mqc_Set_MotorA(0,mqc_Speed_Lelf) funtion
					*/
					mqc_Speed_Right = Speed + abs(mqc_Real_Cal);
					mqc_Speed_Lelf  = 0;
				}	
		  else
			  {
					mqc_Speed_Right = Speed + abs(mqc_Real_Cal);
					mqc_Speed_Lelf  = Speed - abs(mqc_Real_Cal)/Ki;
			  }		
		}

	if(mqc_Error == 0)
		{
			mqc_Speed_Right = Speed;
			mqc_Speed_Lelf  = Speed;
		}
		
	if(mqc_Error > 0)
		{ 
			if(mqc_Line_Status== 1)
			 {
				  /*
    				For out of line, the robot need to turn with high rotational acceleration 
				    if robot can't go back to line, you can set the Right motor to revert by using 
						mqc_Set_MotorA(0,mqc_Speed_Right) or mqc_Set_MotorA(0,mqc_Speed_Lelf) funtion
				  */
					mqc_Speed_Right = 0;
					mqc_Speed_Lelf  = Speed + abs(mqc_Real_Cal);			 
			 }	 
			 else
			 {
					mqc_Speed_Right = Speed - abs(mqc_Real_Cal)/Ki;
					mqc_Speed_Lelf  = Speed + abs(mqc_Real_Cal);			 
			 }

		}	
		
		
		if(mqc_Speed_Right < 0 ) mqc_Speed_Right = 0;
		if(mqc_Speed_Lelf < 0) mqc_Speed_Lelf = 0;
		if(mqc_Speed_Right > 500 ) mqc_Speed_Right = 500;
		if(mqc_Speed_Lelf > 500) mqc_Speed_Lelf = 500;		
	
	mqc_Set_MotorA(1, mqc_Speed_Right);
	mqc_Set_MotorB(1, mqc_Speed_Lelf);		
	}
