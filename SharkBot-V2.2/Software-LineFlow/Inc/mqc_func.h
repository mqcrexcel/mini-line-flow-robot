/*
File:           mqc_func.h
Funtion:        User funtion 
Decription:     This funtion will be control 7SEG LED 
Author:         MQ-Crexcel
Date modefine:  21/01/21
*/


/*Control Motor A 
  dir = 1 run forward, dir = 0 run revert
  pwm min = 0, pwm max = 500
*/
void mqc_Set_MotorA(int dir, int pwm);


/*Control Motor A 
  dir = 1 run forward, dir = 0 run revert
  pwm min = 0, pwm max = 500
*/
void mqc_Set_MotorB(int dir, int pwm);

/*Stop Motor A*/
void mqc_Stop_MotorA(void);

/*Stop Motor B*/
void mqc_Stop_MotorB(void);

void mqc_Read_Button(void);

void mqc_Calib_Line(void);

void mqc_Compare_Sensor(void);

void mqc_Show_Led(void);

void mqc_Calc_Speed(int Kp,int Ki,int Speed);

void mqc_PID(int Kp, int Ki, int Kd, int Speed);
